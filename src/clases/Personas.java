package clases;


import Window.Ventana;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.itextpdf.text.*;
import java.io.*;
import java.util.logging.*;

public class Personas {

    private String nombre;
    private String apellido;
    private String dni;
    private String tel;
    private String mail;

    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void parse() {
        JsonObject arr = this.reader();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        try (Writer writer = new FileWriter("Personas.json")) {
            arr.add(dni + "", gson.toJsonTree(this));

            gson.toJson(arr, writer);
            //persona1.parse();
        } catch (IOException ex) {
            Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public JsonObject reader() {

        JsonObject arr = new JsonObject();
        try (Reader reader = new FileReader("Personas.json")) {
            Gson gson = new Gson();
            arr = gson.fromJson(reader, new TypeToken<JsonObject>() {
            }.getType());
        } catch (IOException e) {
            try {
                FileWriter fw = new FileWriter("Personas.json");
            } catch (IOException ex) {
                Logger.getLogger(Personas.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return arr;
    }
    
}
