package clases;


import Window.Ventana;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import java.io.*;
import java.util.*;
import java.util.logging.*;

public class MetodoPago {

    private String metododepago;
    private String ntarjeta;
    private String mesfechacaducidad;
    private String añofechacaducidad;
    private String codseguridad;
    private String infonombre;
    private String infoapellido;
    private String dirfacturacion;
    private String localidad;
    private String cpzip;
    private String pais;
    private String tel;
    private String dni;

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getMetododepago() {
        return metododepago;
    }

    public void setMetododepago(String metododepago) {
        this.metododepago = metododepago;
    }

    public String getNtarjeta() {
        return ntarjeta;
    }

    public void setNtarjeta(String ntarjeta) {
        this.ntarjeta = ntarjeta;
    }

    public String getMesfechacaducidad() {
        return mesfechacaducidad;
    }

    public void setMesfechacaducidad(String mesfechacaducidad) {
        this.mesfechacaducidad = mesfechacaducidad;
    }

    public String getAñofechacaducidad() {
        return añofechacaducidad;
    }

    public void setAñofechacaducidad(String añofechacaducidad) {
        this.añofechacaducidad = añofechacaducidad;
    }

    public String getCodseguridad() {
        return codseguridad;
    }

    public void setCodseguridad(String codseguridad) {
        this.codseguridad = codseguridad;
    }

    public String getInfonombre() {
        return infonombre;
    }

    public void setInfonombre(String infonombre) {
        this.infonombre = infonombre;
    }

    public String getInfoapellido() {
        return infoapellido;
    }

    public void setInfoapellido(String infoapellido) {
        this.infoapellido = infoapellido;
    }

    public String getDirfacturacion() {
        return dirfacturacion;
    }

    public void setDirfacturacion(String dirfacturacion) {
        this.dirfacturacion = dirfacturacion;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getCpzip() {
        return cpzip;
    }

    public void setCpzip(String cpzip) {
        this.cpzip = cpzip;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

public void parse() {
        JsonObject arr = this.reader();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        try (Writer writer = new FileWriter("MetodoPago.json")) {
            arr.add(dni+"", gson.toJsonTree(this));

            gson.toJson(arr, writer);
            //persona1.parse();
        } catch (IOException ex) {
            Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public JsonObject reader() {

        JsonObject arr = new JsonObject();
        try (Reader reader = new FileReader("MetodoPago.json")) {
            Gson gson = new Gson();
            arr = gson.fromJson(reader, new TypeToken<JsonObject>() {
            }.getType());
        } catch (IOException e) {
            try {
                FileWriter fw = new FileWriter("MetodoPago.json");
            } catch (IOException ex) {
                Logger.getLogger(Personas.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return arr;
    }
}
