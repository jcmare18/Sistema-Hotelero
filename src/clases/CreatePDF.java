package clases;


import clases.CheckIn;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.*;
import java.util.logging.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author juan_cruz
 */
public class CreatePDF {

    private Personas persona;
    private CheckIn checkin;
    private MetodoPago metodopago;

    public void CreatePDF(Personas persona, CheckIn checkin, MetodoPago metodopago) {
        this.persona = persona;
        Document document = new Document(PageSize.A4, 70, 55, 100, 55);
        try {
            //FileWriter fw = new FileWriter("/home/juan_cruz/" + this.nombre + ".pdf");
            FileOutputStream output = new FileOutputStream("/home/juan_cruz/Escritorio/"+persona.getNombre() + ".pdf");
            PdfWriter writer = PdfWriter.getInstance(document, output);
            writer.setBoxSize("art", new Rectangle(36, 54, 559, 788));
            document.open();
            document.addTitle("Cliente " + persona.getNombre());
            document.newPage();

            Paragraph titulo = new Paragraph();
            titulo.setAlignment(Paragraph.ALIGN_CENTER);
            titulo.setFont(FontFactory.getFont("Times New Roman", 24, Font.BOLD, BaseColor.RED));
            titulo.add("Documento para el cliente");

            Paragraph ClienteId = new Paragraph();
            ClienteId.setAlignment(Paragraph.ALIGN_LEFT);
            ClienteId.setFont(FontFactory.getFont("Times New Roman", 10, Font.ITALIC, BaseColor.GRAY));
            ClienteId.add("Cliente ID: " + persona.getDni());

            Paragraph DatosUsuario = new Paragraph();
            DatosUsuario.setAlignment(Paragraph.ALIGN_LEFT);
            DatosUsuario.setFont(FontFactory.getFont("Times New Roman", 17, Font.UNDERLINE, BaseColor.DARK_GRAY));
            DatosUsuario.add("Datos del Cliente");

            Paragraph Checkin = new Paragraph();
            Checkin.setAlignment(Paragraph.ALIGN_LEFT);
            Checkin.setFont(FontFactory.getFont("Times New Roman", 17, Font.UNDERLINE, BaseColor.DARK_GRAY));
            Checkin.add("Informacion del Check-in");

            Paragraph MetodoPago = new Paragraph();
            MetodoPago.setAlignment(Paragraph.ALIGN_LEFT);
            MetodoPago.setFont(FontFactory.getFont("Times New Roman", 17, Font.UNDERLINE, BaseColor.DARK_GRAY));
            MetodoPago.add("Infomracion del Metodo de Pago");

            document.add(titulo);
            document.add(ClienteId);

            document.add(new Paragraph("-----------------------------------------------------------------------"
                    + "----------------------------------------------"));
            document.add(DatosUsuario);
            document.add(new Paragraph("Nombre: " + persona.getNombre() + ", " + persona.getApellido()));
            document.add(new Paragraph("Dni: " + persona.getDni() + ""));
            document.add(new Paragraph("Telefono: " + persona.getTel() + ""));
            document.add(new Paragraph("Mail: " + persona.getMail()));
            document.add(new Paragraph("-----------------------------------------------------------------------"
                    + "----------------------------------------------"));
            document.add(Checkin);
            document.add(new Paragraph("1)Tipo de habitacion: " + checkin.getTipohabit1()));
            document.add(new Paragraph("1)Cantidad de habitaciones: " + checkin.getCanthabit1()));

            if (checkin.getTipohabit2() != "--------") {
                document.add(new Paragraph("2)Tipo de habitacion: " + checkin.getTipohabit2()));
                document.add(new Paragraph("2)Cantidad de habitaciones: " + checkin.getCanthabit2()));
            } else {

            }
            if (checkin.getTipohabit3() != "--------") {
                document.add(new Paragraph("3)Tipo de habitacion: " + checkin.getTipohabit3()));
                document.add(new Paragraph("3)Cantidad de habitaciones: " + checkin.getCanthabit3()));
            } else {

            }

            document.add(new Paragraph("Fecha de ingreso: " + checkin.getFingreso()));

            document.add(new Paragraph("Fecha de salida: " + checkin.getFsalida()));
            document.add(new Paragraph("Cantidad de adultos: " + checkin.getAdultos()));
            document.add(new Paragraph("Cantidad de menores: " + checkin.getMenores()));
            String cochera;
            if (checkin.getCochera() == true) {
                cochera = "Si desea cochera";
            } else {
                cochera = "No desea cochera";
            }
            document.add(new Paragraph("Cochera: " + cochera));
            document.add(new Paragraph("-----------------------------------------------------------------------"
                    + "----------------------------------------------"));
            document.add(MetodoPago);
            document.add(new Paragraph("Nombre " + metodopago.getInfonombre() + ", " + metodopago.getInfoapellido()));
            document.add(new Paragraph("Localidad: " + metodopago.getLocalidad()));
            document.add(new Paragraph("Codigo postal o zip: " + metodopago.getCpzip()));
            document.add(new Paragraph("Pais: " + metodopago.getPais()));
            document.add(new Paragraph("Telefono: " + metodopago.getTel()));
            document.add(new Paragraph("Metodo de pago: " + metodopago.getMetododepago()));
            document.add(new Paragraph("N° de tarjeta: " + metodopago.getNtarjeta()));

            document.close();
        } catch (IOException | DocumentException ex) {
            Logger.getLogger(Personas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
