package clases;


import Window.Ventana;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import java.io.*;
import java.util.*;
import java.util.logging.*;

public class CheckIn {

    private String tipohabit1;
    private String tipohabit2;
    private String tipohabit3;
    private String canthabit1;
    private String canthabit2;
    private String canthabit3;
    private String fingreso;
    private String fsalida;
    private String adultos;
    private String menores;
    private Boolean cochera;
    private String dni;

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    
    public String getTipohabit1() {
        return tipohabit1;
    }

    public void setTipohabit1(String tipohabit1) {
        this.tipohabit1 = tipohabit1;
    }

    public String getTipohabit2() {
        return tipohabit2;
    }

    public void setTipohabit2(String tipohabit2) {
        this.tipohabit2 = tipohabit2;
    }

    public String getTipohabit3() {
        return tipohabit3;
    }

    public void setTipohabit3(String tipohabit3) {
        this.tipohabit3 = tipohabit3;
    }

    public String getCanthabit1() {
        return canthabit1;
    }

    public void setCanthabit1(String canthabit1) {
        this.canthabit1 = canthabit1;
    }

    public String getCanthabit2() {
        return canthabit2;
    }

    public void setCanthabit2(String canthabit2) {
        this.canthabit2 = canthabit2;
    }

    public String getCanthabit3() {
        return canthabit3;
    }

    public void setCanthabit3(String canthabit3) {
        this.canthabit3 = canthabit3;
    }

    public String getFingreso() {
        return fingreso;
    }

    public void setFingreso(String fingreso) {
        this.fingreso = fingreso;
    }

    public String getFsalida() {
        return fsalida;
    }

    public void setFsalida(String fsalida) {
        this.fsalida = fsalida;
    }

    public String getAdultos() {
        return adultos;
    }

    public void setAdultos(String adultos) {
        this.adultos = adultos;
    }

    public String getMenores() {
        return menores;
    }

    public void setMenores(String menores) {
        this.menores = menores;
    }

    public Boolean getCochera() {
        return cochera;
    }

    public void setCochera(Boolean cochera) {
        this.cochera = cochera;
    }
    public void parse() {
        JsonObject arr = this.reader();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        try (Writer writer = new FileWriter("Check-in.json")) {
            arr.add(dni+"", gson.toJsonTree(this));

            gson.toJson(arr, writer);
            //persona1.parse();
        } catch (IOException ex) {
            Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public JsonObject reader() {

        JsonObject arr = new JsonObject();
        try (Reader reader = new FileReader("Check-in.json")) {
            Gson gson = new Gson();
            arr = gson.fromJson(reader, new TypeToken<JsonObject>() {
            }.getType());
        } catch (IOException e) {
            try {
                FileWriter fw = new FileWriter("Check-in.json");
            } catch (IOException ex) {
                Logger.getLogger(Personas.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return arr;
    }
}
