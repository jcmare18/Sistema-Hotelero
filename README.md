# Sistema Hotelero

### DESCRIPCIÓN

Este programa es un sistema de administración hotelero, mediante el cual se van a poder cargar todos los datos de la persona que desee alojarse en el hotel,
como también los datos de el check-in (tipo de habitación, dias de estadia, etc). También tendrá una parte donde se le tomará al cliente el dato del pago y cómo desea pagar,
todos los datos se guardaran en 3 archivos JSON, 1 por cada clase(persona,check-in,metodopago), luego a partir de este se creará una factura(pdf)con todos los datos del pedido.


### VENTANA Y FUNCIONALIDADES

El programa tiene tres ventanas, en la primera se pediran los datos del usuario(nombre, apellido, dni, tel, mail). En la segunda se tomaran los datos de check-in al usuario(tipo de habitacion, cantidad de habitaciones, fecha de ingreso, salida, la cantidad de adultos y menores y si desea cochera o no). En la tercera se pediran todos los datos para realizar el pago(metodo de pago, n de tarjeta, datos de la persona que paga, fecha de vencimiento y codigo de seguridad de la tarjeta)

![1](/uploads/941b07259edc41e60df7d17208a3e193/1.png)

![2](/uploads/da858d81724a6e868fd29cd7c002e464/2.png)

![3](/uploads/8dedf2461b7eb27fa22a3015ca99aa1c/3.png)

### DEPENDENCIAS

#### jcalendar: https://goo.gl/8P9yCF

#### GSON: https://goo.gl/s8HPBV

### itextPdf: https://goo.gl/Fiiyi1

### INTEGRANTES

Cuadrado Valdez, Antonio

Mare, Juan Cruz
